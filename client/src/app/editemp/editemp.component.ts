import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-editemp',
  templateUrl: './editemp.component.html',
  styleUrls: ['./editemp.component.css']
})
export class EditempComponent implements OnInit {

  public eid = '';
  public searchRes = {}; 
  public formDetails = {
    _id: "",
    name: "",
    e_id: "",
    dept: "",
    salary: "",
    email: "",
    phone: ""
  };
  public successmodal: boolean;
  public errormodal: boolean;
  public view: boolean;

  constructor(private http: Http, private route: ActivatedRoute) { }

  ngOnInit() {
    this.successmodal = false;
    this.errormodal = false;
    this.view = true;
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.eid = params.get('eid');
      //console.log(this.e_id);
    });
    this.getDetails();
  }

  getDetails(){
    let url = "http://localhost:3000/api/searchid";
    this.http.get(url + '/' +  this.eid)
      .subscribe(res => {
        this.searchRes = res.json();
        //console.log(this.searchRes);
        this.setFromValues();
      });
  }

  setFromValues(){
    this.formDetails._id = this.searchRes[0]._id;
    this.formDetails.name = this.searchRes[0].name;
    this.formDetails.e_id = this.searchRes[0].e_id;
    this.formDetails.dept = this.searchRes[0].dept;
    this.formDetails.salary = this.searchRes[0].salary;
    this.formDetails.email = this.searchRes[0].contact.email;
    this.formDetails.phone = this.searchRes[0].contact.phone;
  }
  
  onSubmit(value){
    let url = "http://localhost:3000/api/update";
    this.http.post(url, this.formDetails)
      .subscribe(res => {
        console.log(res.json());
        let success = res.json();
        if(success.success == "success"){
          this.successmodal = !this.successmodal;
          this.view = !this.view;
        }else{
          this.errormodal = !this.errormodal;
          this.view = !this.view;
        }
      })
  }
}
