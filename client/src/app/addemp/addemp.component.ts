import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router'

@Component({
  selector: 'app-addemp',
  templateUrl: './addemp.component.html',
  styleUrls: ['./addemp.component.css']
})
export class AddempComponent implements OnInit {
  private errormodal: boolean;
  private successmodal: boolean;
  private view: boolean;

  constructor(private http:Http, private router:Router) { }

  ngOnInit() {
    this.errormodal = false;
    this.successmodal = false;
    this.view = true;
  }

  onSubmit(value){
  	// console.log(value.name);
  	let url = "http://localhost:3000/api";
  	this.http.post(url, value).subscribe(res => {
      console.log(res.json());
      let data = res.json();
      if(data.success == "success"){
        this.successmodal = !this.successmodal;
        this.view = !this.view;
      }
      else{
        this.errormodal = !this.errormodal;
        this.view = !this.view;
      }
    });
  }

  

  retry(){
    this.errormodal = !this.errormodal;
    this.view = !this.view;
  }
}
