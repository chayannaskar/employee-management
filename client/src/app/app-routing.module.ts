import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { EditempComponent } from './editemp/editemp.component';
import { AddempComponent } from './addemp/addemp.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
    {path: '', redirectTo: '/addemp', pathMatch: 'full'},
    {path: 'editemp/:eid', component: EditempComponent},
    {path: 'addemp', component: AddempComponent},
    {path: 'searchemp', component: SearchComponent},
    //{path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule{}
export const routingComponents = [EditempComponent, AddempComponent, SearchComponent]