import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchRes = {};
  visible = false;

  constructor(private http:Http, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(value){
    console.log(value);
    if(value.searchRadios == "option1"){
      let url = "http://localhost:3000/api/searchid";
      this.http.get(url + '/' +  value.searchdata)
        .subscribe(res => {
          this.searchRes = res.json();
          console.log(this.searchRes[0]);
        });
    }
    else if(value.searchRadios == "option2"){
      let url = "http://localhost:3000/api/searchname";
      this.http.get(url + '/' +  value.searchdata)
        .subscribe(res => {
          this.searchRes = res.json();
          console.log(this.searchRes[0]);
        });
    }
    if(this.searchRes[0] == undefined){
      this.visible = !this.visible;
      console.log(this.visible)
    }
  }

  view_all(){
    let url = "http://localhost:3000/api";
    this.http.get(url)
        .subscribe(res => {
          this.searchRes = res.json();
          console.log(this.searchRes);
    });
  }

  deletedata(value){
    console.log(value);
    let url = "http://localhost:3000/api/delete";
    this.http.get(url + '/' + value)
        .subscribe(res => {
          this.searchRes = res.json();
          console.log(this.searchRes);
    });
    this.view_all();
  }

  editdata(value){
    //console.log(value);
    this.router.navigate(['/editemp', value])
  }
}
