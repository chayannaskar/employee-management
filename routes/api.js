var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var db = require('../database')

var EmployeeModel = require('../models/employee');

router.get('/', (req, res, next) => {
	EmployeeModel.find()
		.then(doc => {
			res.json(doc);
		})
		.catch(err => {
			console.log(err);
			res.json({error: "error"});
		})
})

router.post('/', (req, res) => {
	// console.log(req.body.name);
	let emp = new EmployeeModel({
		e_id: req.body.e_id,
		name: req.body.name,
		dept: req.body.dept,
		salary: req.body.salary,
		contact: {
			phone: req.body.phone,
			email: req.body.email
		},
	})

	emp.save()
	   .then(doc => {
	     res.json({success: "success"});
	   })
	   .catch(err => {
	    	console.log(err);
			res.json({error: "error"});
	   })
})

router.get('/searchid/:id', (req, res) => {
	console.log(req.params.id)
	EmployeeModel.find({e_id: req.params.id})
		.then(doc => {
			//console.log(doc)
			res.json(doc);
		})
		.catch(err => {
			console.log(err)
			res.json({error: "error"});
		})
})

router.get('/searchname/:name', (req, res) => {
	// console.log(req.body.searchdata)
	EmployeeModel.find({name: req.params.name})
		.then(doc => {
			//console.log(doc)
			res.json(doc);
		})
		.catch(err => {
			console.log(err);
			res.json({error: "error"})
		})
})

router.get('/delete/:id', (req, res) => {
	// console.log(req.body.searchdata)
	EmployeeModel.findOneAndRemove({e_id: req.params.id})
		.then(doc => {
			console.log(doc)
			//res.json(doc);
		})
		.catch(err => {
			console.log(err);
			res.json({error: "error"})
		})
})

router.post('/update', (req, res) => {
	EmployeeModel.findOneAndUpdate({
		_id: req.body._id
	}, {
		e_id: req.body.e_id,
		name: req.body.name,
		dept: req.body.dept,
		salary: req.body.salary,
		contact:{
			phone: req.body.phone,
			email: req.body.email,
		}
	})
		.then(doc => {
			console.log(doc);
			res.json({success: "success"});
			//res.json(doc);
		})
		.catch(err => {
			console.log(err);
			res.json({error: "error"})
		})
})


module.exports = router;