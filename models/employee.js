let mongoose = require('mongoose')
let validator = require('validator')

let employeeSchema = new mongoose.Schema({
	e_id: Number,
	name: String,
	dept: String,
	salary: Number,
	contact: {
		phone: Number,
		email: {
			type:String,
			lowercase: true,
			validate: (value) => {
				return validator.isEmail(value)
			}
		}
	}
})

module.exports = mongoose.model('Employee', employeeSchema)